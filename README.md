# Deloitte Assignment Week 2

#### Exercise 1:

**Develop a calculator application to perform addition, subtraction, multiply and divide two number based on the choice1 add 2 sub 3 mul 4 div without using any control statement *like if, else if, case***

* Create an abstract class Arithmetic with has 2 instance variable num1, num2. Methods to read, and display with the constructor to initialize. Abstract method calculate
* Create sub class for addition, subtraction, multiplication and divide and override calculate method.
*	Create a array of reference for arithmetic class and initialize object of sub call (addition, subtraction, multiplication and division)
*	Read two numbers and choice perform the calculation base on choice without using control statement.
**Calculator.java**

```java
import java.util.Scanner;

public class Calculator {

	public static void main(String[] args) {
		
		Object[] fs = {new Add(), new Sub(), new Mult(), new Div(), new Exit()};
		System.out.println("Enter your choice:\n1. Add\n2. Sub\n3. Multiply\n4. Divide\n5. Exit");
		int input;
		Scanner sc = new Scanner(System.in);
		input = sc.nextInt();
		int num1, num2;//  = 12, num2 = 6;
		System.out.println("Enter two numbers: ");
		num1 = sc.nextInt();
		num2 = sc.nextInt();
		int y = ((Arithmetic)fs[input-1]).calc(num1, num2);
		System.out.println(y);
		sc.close();

	}
}
```

#### Exercise 2:  

**Problem Statement:Develop java application to compute income tax of employee based on annual income.Create class EmplyeeVo which contain instance variable empid, empname, annualincome, incometax. Create setter and getter methods**

*Override the following methods:*
* toString method which returns all the employee details.
*	Hashcode method to return the hashcode of the data
*	Equals method to compare the object
*	Add the parameter constructor to add data


Create class EmplyeeBo which contains instance method calincomeTax (), return type is void, argument is EmplyeeVo. Take the annualincome for emplyeeVo and compute income tax and store it back to emplyeeVo.

Create class Emplyeesort implement comparator interface and override compareTo method do sort the data on incometax in descending order.
Create an EmplyeeMain with main method and do the following***
*	Accept no of employees
*	Create an array of EmplyeeVo based on number of employees
*	Read empid, empname, annualincome and calculate income tax calling calincomeTax method of EmplyeeBo object.
*	Display the data of the all the employee
*	Sort the data of the employee based on income tax and re print.


# Exercise 3:  

**Given an integer array, Write a program to find if the array has any triplets. A triplet is a value if it appears 3 consecutive times in the array.**

***Include a class UserMainCode with a static method checkTripplets which accepts an integer array. The return type is boolean stating whether its a triplet or not.***

***Create a Class Main which would be used to accept the input arrayand call the static method present in UserMainCode.***

***Input and Output Format:
Input consists of n+1 integers. The first integer would represent the size of array and the next n integers would have the values.
Output consists of a string stating TRUE or FALSE.
Refer sample output for formatting specifications.***

*Sample Input 1:*
7
3
3
5
5
5
2
3

*Sample Output 1:*
TRUE

*Sample Input 2:*
7
5
3
5
1
5
2
3

*Sample Output 2:*
FALSE


```java
import java.util.*;

public class Triplets {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println(" Enter the size ?");
		int n = sc.nextInt();
		int arr[] = new int[n + 1];
		arr[0] = n;
		System.out.println(" Enter the elements?");
		for (int i = 1; i < n + 1; i++) {
			arr[i] = sc.nextInt();
		}
		System.out.print(checkTriplets(arr));

	}

	public static boolean checkTriplets(int arr[]) {
		for (int i = 1; i < (arr.length) - 1; i++) {
			if (arr[i - 1] == arr[i] && arr[i] == arr[i + 1])
				return true;
		}
		return false;

	}

}
```