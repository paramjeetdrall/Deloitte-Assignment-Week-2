import java.util.*;

public class Triplets {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println(" Enter the size ?");
		int n = sc.nextInt();
		int arr[] = new int[n + 1];
		arr[0] = n;
		System.out.println(" Enter the elements?");
		for (int i = 1; i < n + 1; i++) {
			arr[i] = sc.nextInt();
		}
		System.out.print(checkTriplets(arr));

	}

	public static boolean checkTriplets(int arr[]) {
		for (int i = 1; i < (arr.length) - 1; i++) {
			if (arr[i - 1] == arr[i] && arr[i] == arr[i + 1])
				return true;
		}
		return false;

	}

}
